const curveHeading = () => {
  const curvedHeadings = document.querySelectorAll('.js-curved-heading');

  if (curvedHeadings) {
    console.log(curvedHeadings)
    curvedHeadings.forEach(text => {
      const circleType = new CircleType(text);
      circleType.radius(260);

      if (window.innerWidth < 600) {
        circleType.radius(190);
      }
      else if (window.innerWidth < 400){
        circleType.radius(100);
      }
    })
  }
}

window.onload = () => {
  console.log(1);
  curveHeading();
}